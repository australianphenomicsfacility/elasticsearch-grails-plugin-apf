package org.grails.plugins.elasticsearch;

public interface ScrollHandler {
	public void handleScroll(def domainInstances);
}
